const { checkLength, checkAlphabet, checkDigit, checkSymbol, checkPassword } = require('./password')
describe('Test Password Length', () => {
  test('should 8 characters to be true', () => {
    expect(checkLength('12345678')).toBe(true)
  })

  test('should 7 characters to be false', () => {
    expect(checkLength('1234567')).toBe(false)
  })

  test('should 25 characters to be true', () => {
    expect(checkLength('1111111111111111111111111')).toBe(true)
  })

  test('should 26 characters to be false', () => {
    expect(checkLength('11111111111111111111111111')).toBe(false)
  })
})

describe('Test Alphabet', () => {
  test('should has alphabet m in password', () => {
    expect(checkAlphabet('m')).toBe(true)
  })

  test('should has alphabet A in password', () => {
    expect(checkAlphabet('A')).toBe(true)
  })

  test('should has not alphabet in password', () => {
    expect(checkAlphabet('1111')).toBe(false)
  })
})

describe('Test Digit', () => {
  test('should has digit in password to be true', () => {
    expect(checkDigit('1234')).toBe(true)
  })
  test('should has not  digit in password to be false', () => {
    expect(checkDigit('abc')).toBe(false)
  })
})

describe('Test Symbol', () => {
  test('should has symbol ! in password to be true', () => {
    expect(checkSymbol('11!11')).toBe(true)
  })
  test('should has not symbol ! in password to be false', () => {
    expect(checkSymbol('1111')).toBe(false)
  })
})

describe('Test Password', () => {
  test('should password cde@ to be false', () => {
    expect(checkPassword('cde@')).toBe(false)
  })
  test('should password warm@6666 to be true', () => {
    expect(checkPassword('warm@6666')).toBe(true)
  })

})
